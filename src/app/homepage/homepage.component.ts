import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  model: any = {};
  submitted = false;
  error: {};
  res_contact: string;

  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
  }

  onSubmit(contactForm:any){
    var baseurl = 'http://localhost/adnan/ci_projects';
    var contactname = contactForm.form.value.name;
    var contctemail = contactForm.form.value.useremail;
    var contactnumber = contactForm.form.value.phone;
    var contactmessage = contactForm.form.value.message;
    // console.log("name- "+fullName + "--email --- "+ email + "--- number --- "+ phone+"---message----"+ message);
    let contact_data = {
      name: contactname,
      email: contctemail,
      phone: contactnumber,
      message: contactmessage
    }

    console.log(contact_data);

    this.http.post(baseurl + "/v1/contact/saveContact", contact_data).subscribe(res => {
      // console.log(res);
      if(res['msg'] == "Success."){
        this.res_contact = 'Thank You! Your Request Has Been Submitted. We will get back to you soon.';
        contactForm.reset();
      }
    },(err) => {
      // console.log(err)
      this.res_contact = err;
    });


  }

}
